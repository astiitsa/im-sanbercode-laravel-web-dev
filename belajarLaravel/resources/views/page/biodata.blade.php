<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up Form</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/kirim" method="post">
    @csrf
    <label>Nama Depan</label><br>
    <input type="text" name="fname"><br><br>
    <label>Nama Belakang</label><br>
    <input type="text" name="lname"><br><br>
    <label>Gender :</label><br>
    <input type="radio" name="gender" value="male"> Male <br>
    <input type="radio" name="gender" value="female"> Female <br>
    <input type="radio" name="gender" value="other"> Other <br>
    <br>
    <label>Nationality :</label><br>
    <select name="nationality" id="">
        <option value="1">Indonesian</option>
        <option value="2">Singapore</option>
        <option value="3"> Malaysia</option>
        <option value="4">Australia</option>
    </select><br><br>
    <label>Language Spoken :</label> <br>
    <input type="checkbox" name="language" value="1"> Bahasa Indonesia <br>
    <input type="checkbox" name="language" value="2">English <br>
    <input type="checkbox" name="language" value="3">Other <br>
    <br>
    <label>Bio :</label><br>
    <textarea name="bio" id="" cols="30" rows="10"></textarea><br><br>
    <input type="submit" value="kirim">
    </form>
</body>
</html>