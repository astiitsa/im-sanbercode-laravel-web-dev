@extends('layouts.master')
@section('title')
    Halaman List Cast
@endsection
@section('subtitle')
    List Cast
@endsection
@section('content')
<a href="/cast/create" class="btn btn-primary btn-sm my-2">Tambah</a>

        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">Umur</th>
                <th scope="col">Bio</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($cast as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->nama}}</td>
                        <td>{{$value->umur}}</td>
                        <td>{{$value->bio}}</td>
                        <td>
                            <form action="/cast/{{$value->id}}" method="POST">
                            @csrf
                            @method('delete')
                            <a href="/cast/{{$value->id}}" class="btn btn-info btn-sm">Detail</a>
                            <a href="/cast/{{$value->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                            <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                            </form>
                        </td>     
                    </tr>
                @empty
                    <tr colspan="5">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>

@endsection