<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BiodataController extends Controller
{
    public function bio()
    {
        return view('page.biodata');
    }
    public function welcome(Request $request)
    {
        $namadepan = $request['fname'];
        $namabelakang = $request['lname'];

        return view('page.home',['namadepan' => $namadepan, 'namabelakang' => $namabelakang]);
    }
}
